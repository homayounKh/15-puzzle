class Board {
    blockWidthPx = 0;
    blockHeightPx = 0;
    blockRowNum = 0;
    blockColNum = 0;

    numbersList = [];
    blocksList = [];
    blocksLen = 0;

    nullBlockIndex = 0;
    nullBLockSideIds = {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
    }

    #isMoving = false;

    constructor(blockWidthPx, blockHeightPx, blockRowNum, blockColNum) {
        this.blockWidthPx = blockWidthPx;
        this.blockHeightPx = blockHeightPx;
        this.blockRowNum = blockRowNum;
        this.blockColNum = blockColNum;
        this.blocksLen = this.blockRowNum*this.blockColNum;

        document.documentElement.style.setProperty('--blockColNum', blockColNum)
        document.documentElement.style.setProperty('--blockRowNum', blockRowNum)
        document.documentElement.style.setProperty('--blockWidthPx', blockWidthPx)
        document.documentElement.style.setProperty('--blockHeightPx', blockHeightPx)
    }

    generatePuzzle(){
        this.initBlocksList();
        this.updateNullBLockSideIds();
        this.generateBlocks();
    }

    updateBoard() {
        this.blocksLen = this.blockRowNum*this.blockColNum;
        document.documentElement.style.setProperty('--blockColNum', this.blockColNum)
        document.documentElement.style.setProperty('--blockRowNum', this.blockRowNum)
        document.documentElement.style.setProperty('--blockWidthPx', this.blockWidthPx)
        document.documentElement.style.setProperty('--blockHeightPx', this.blockHeightPx)
        this.generateBlocks();
    }

    initBlocksList() {
        this.numbersList = Array.from(Array(this.blocksLen).keys());
        let numbersListTemp = [...this.numbersList];

        let i = this.blocksLen;
        let j = 0;
        while (i--) {
            j = Math.floor(Math.random() * (i+1));
            this.blocksList.push(numbersListTemp[j]);
            if(numbersListTemp[j] == 0){
                this.nullBlockIndex = this.blocksList.length-1;
            }
            numbersListTemp.splice(j,1);
        }
    }

    updateNullBLockSideIds() {
        let top = this.nullBlockIndex - this.blockColNum;
        let right = this.nullBlockIndex + 1;
        let bottom = this.nullBlockIndex + this.blockColNum;
        let left = this.nullBlockIndex - 1;

        this.nullBLockSideIds = {
            top: top >= 0 ? this.blocksList[top] : 0,
            right: right % this.blockColNum !== 0 ? this.blocksList[right] : 0,
            left: (left+1) % this.blockColNum !== 0 ? this.blocksList[left] : 0,
            bottom: bottom < this.blocksLen ? this.blocksList[bottom] : 0,
        }
    }

    resetBoard() {
        this.blockWidthPx = 0;
        this.blockHeightPx = 0;
        this.blockRowNum = 0;
        this.blockColNum = 0;

        this.blocksList = [];
        this.blocksLen = 0;

        this.nullBlockIndex = 0;
        this.nullBLockSideIds = {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
        }

        this.#isMoving = false;
    }

    generateBlocks() {
        let block = document.getElementById('sample-block-layout').children[0];
        let board = document.getElementById('board');

        for (let i = 0; i < this.blocksList.length; i++) {
            block.children[0].classList.remove('null');
            block.children[0].id = this.blocksList[i];
            block.getElementsByTagName('span')[0].innerText = this.blocksList[i];
            if(this.blocksList[i] === 0) {
                block.children[0].classList.add('null')
            }
            board.innerHTML += block.parentElement.innerHTML;
        }
    }

    updateBlocks(blockId) {
        let nullBlock;
        let block;
        nullBlock = document.getElementById('0');
        block = document.getElementById(blockId);

        nullBlock.getElementsByTagName('span')[0].innerText = blockId;
        block.getElementsByTagName('span')[0].innerText = 0;

        nullBlock.id = blockId;
        block.id = 0;

        block.classList.add('null')
        nullBlock.classList.remove('null');
    }

    move(el, blockId, direction) {
        return new Promise((resolve, reject) => {
            if(this.#isMoving)
                reject();

            let trans =
                direction === 'bottom' ? 'to-bottom-transition' :
                    direction === 'left' ? 'to-left-transition' :
                        direction === 'top' ? 'to-top-transition' :
                            direction === 'right' ? 'to-right-transition' : '';

            let root = this;
            el.classList.add(trans)

            let listener = function () {
                let blockIndex = root.blocksList.indexOf(blockId);
                root.blocksList[root.nullBlockIndex] = blockId;
                root.blocksList[blockIndex] = 0;
                root.nullBlockIndex = blockIndex;
                root.updateNullBLockSideIds();
                root.updateBlocks(blockId);
                el.classList.remove(trans)

                el.removeEventListener('transitionend', listener);
                root.#isMoving = false;


                if(root.isCorrect())
                    resolve(true);
                else
                    resolve(false);
            };
            el.addEventListener('transitionend', listener);
            this.#isMoving = true;
        })
    }

    isCorrect() {
        let numbersListTmp = [...this.numbersList];
        numbersListTmp.splice(0, 1);
        let correctStr = numbersListTmp.join('') + '0';
        let blocksStr = this.blocksList.join('');

        return correctStr === blocksStr
    }
}

