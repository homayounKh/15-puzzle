window.addEventListener('load', function () {

})

var addModal = document.getElementById('add-modal');
addModal.addEventListener('click', function(event) {
    if(event.target === this)
        addModal.style.display = 'none';

    else if(event.target.id === 'add-puzzle-btn-modal')
        addPuzzle();

    else if(event.target.id === 'cancel-puzzle-btn')
        addModal.style.display = 'none';
})

var winModal = document.getElementById('win-modal');
winModal.addEventListener('click', function(event) {
    if (event.target != winModal.children[0])
        winModal.style.display = 'none'
})

let boards = [];
let currentPuzzleId = 0;
function addPuzzle() {
    let col = document.getElementById('input-col-num').value;
    let row = document.getElementById('input-row-num').value;
    let w = document.getElementById('input-block-width-px').value;
    let h = document.getElementById('input-block-height-px').value;

    document.getElementById('input-col-num').value = '';
    document.getElementById('input-row-num').value = '';
    document.getElementById('input-block-width-px').value = '';
    document.getElementById('input-block-height-px').value = '';

    const board = new Board(w+'px', h+'px', parseInt(row), parseInt(col));
    addModal.style.display = 'none';

    this.addToPuzzleMenu();
    boards.push(board);

    document.getElementById('board').style.visibility = 'visible';
    resetBoard();
    board.generatePuzzle();
}

function addToPuzzleMenu() {
    let puzzleItem = document.getElementById('sample-puzzle-item-layout').children[0];
    puzzleItem.getElementsByTagName('span')[0].innerText = `پازل ${boards.length+1}`;
    puzzleItem.id = `puzzle-${boards.length+1}`

    let html = new DOMParser().parseFromString(puzzleItem.parentElement.innerHTML, 'text/html');
    document.getElementById('add-puzzle-btn').parentElement.after(html.body.children[0]);
    updateMenu(boards.length+1);
}

initPuzzleMenu();
function initPuzzleMenu() {
    document.getElementById('puzzles-menu').addEventListener('click', function (ev) {

        if(ev.target.id === 'add-puzzle-btn') {
            addModal.style.display = 'block'
        }

        else if(ev.target.classList.contains('puzzle-item')) {
            let puzzleId = parseInt(String(ev.target.id).split('-')[1]);
            if(puzzleId === currentPuzzleId)
                return;

            updateMenu(puzzleId);
            resetBoard();
            boards[puzzleId-1].updateBoard();
        }
    });
}

document.getElementById('board').addEventListener('click', async function (ev) {
    if(ev.target.classList.contains('puzzle-block')) {
        let id = parseInt(ev.target.id);
        const board = boards[currentPuzzleId-1];

        let isWin = false;
        for (const key in board.nullBLockSideIds) {
            if(id === board.nullBLockSideIds[key]) {
                if(key === 'top')
                    isWin = await board.move(ev.target, id, 'bottom')
                else if(key === 'right')
                    isWin = await board.move(ev.target, id, 'left')
                else if(key === 'bottom')
                    isWin = await board.move(ev.target, id, 'top')
                else if(key === 'left')
                    isWin = await board.move(ev.target, id, 'right')
                break
            }
        }
        if(isWin)
            winModal.style.display = 'block';
    }
});

function resetBoard() {
    document.getElementById('board').innerHTML = "";
}

function updateMenu(puzzleId) {
    if(currentPuzzleId > 0)
        document.getElementById(`puzzle-${currentPuzzleId}`).classList.remove('selected')
    currentPuzzleId = puzzleId;
    document.getElementById(`puzzle-${currentPuzzleId}`).classList.add('selected')
}







